import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import Reducers from './reducers';
import reduxThunk from 'redux-thunk';


const createMiddleWareStore = applyMiddleware(reduxThunk)(createStore);
const store = createMiddleWareStore(Reducers);

ReactDOM.render(<Provider store={store}><App/></Provider>, document.getElementById('root'));
